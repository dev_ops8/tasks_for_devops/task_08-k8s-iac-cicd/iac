# task_08-k8s-cicd-iac-web-app
This task simulates the real project of dockerized app

## Description of the task
We have small team includes devops group and developer's group. We must create CI/CD pipeline that can do ordinary things such as testing the developer's code, building docker images, push it to our registry and etc. But we must create the CD part, that will deploy our dockerized app to real "dev" environment on cloud provider, trigger by any commit in "<u>dev</u>" branch in <u>app repository</u> via developer's group.
So, our CI/CD pipeline can linting dev code, build docker images, push to registry and deploy it to "dev" environment on cloud provider via any commit in "dev" branch in <u>app repository</u> via developer's group. Our developer's are professional, so they always add tag's when commit to "dev" branch. When our CI/CD pipeline will deploy to cloud provider, we must reuse this tag, as a tag for our deployed apps on cloud provider environment, it gives us possibility to follow for our versioning of app. 
Also, if we must update/change K8s ifrastructure via devops team, we must be able to find the "release" version of infrastructure.
If any wrong will happen and our K8s infra down, we must rollback to previous version. 
Also we must save log's from our app to any persistent volume. It help us to analyze the behaviour of app.
Files for app take from task_04

## Check-points:
- Separate on different repository via purpose of code: app - for app code, IaC - for infra code, K8s - for K8s code
- Create CI/CD pipeline that will linting app code, build images and push to registry by any commint in branch "<u>dev</u>" in app repository
- Create CI/CD pipeline that can deploy/update version of app trigger by commits in <u>app repository</u>
- Create CI/CD pipeline that can deploy/update version of K8s infra trigger by commits in <u>K8s repository</u>  
- Create CI/CD pipeline that can deploy infrastructure in cloud provider trigger by commits in <u>IaC repository</u> 
- Create simple logging system to save log's of pod's(fluentd or etc)

## Intro and general steps of provision infrastructure
This repository define for storage of code for provision our infrastructure use best practice of IaC. Repo consist of terraform files. Also repo has 2 branches: "<u>dev</u>" branch is can use for next develop or bugfix and the "<u>main</u>" branch that can use for deploy to "dev" or "prod" environment. For dev environment, we can use some different variables, which we rewrite by "<u>dev.tfvars</u>" file in our CI/CD pipeline. In our example we don't need to rewrite vars, but it can be need in some situations. For prod environment, we can use "<u>prod.tvfars</u>". Also we must create dev and prod "<u>workspaces</u>" in terraform - that allow us to deploy to different environment use the same code, store ".tfstate" file on S3 bucket for example in different folder dependent on workspace. By this way we control 2 or more environment for our different purpose. 

## Checkpoints:
- Create S3 bucket with dynamo-db for storage .tfstate, after creation activate backend in terraform.tf 
- Create VPC with private and public subnet's
- Create K8s cluster
- Provision loki-graffana bundle to our cluster
- Create S3 bucket for our log's
- Create IAM role's for connection between K8s loki pod's and AWS S3, K8s mysql pod's and AWS EBS

## Notes
- To provision to "dev" environment: terraform workspace select dev && terraform apply -auto-approve -var-file=dev.tfvars
- To provision to "prod" environment: terraform workspace select dev && terraform apply -auto-approve -var-file=prod.tfvars

<p align="center">
  <img src="./diagram_2.png" />
</p>

At this diagramm we can see architecture of our infrustructure. There are "App-pod's" - as a frontend-backend app on flask, which can provide static file's, process api-requests and send sql-request to database instances thorough "ProxySQL-pod's". "ProxySQL-pod's" is use for forwarding sql-requests to databases follow defined write/read rules. Database instances are existing RDS-db. 