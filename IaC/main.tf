#------------------------------------------------
# Steps of provision highly available web-app with db, backend:
#   - vpc.tf - VPC(subnet) for K8s cluster
#   - eks.tf - K8s cluster
#   - irsa-ebs-csi-driver.tf - IAM role for container storage for K8s (can use disk)
#   - irsa-loki.tf - IAM role for loki (can connect to S3 bucket)
#   - s3.tf - S3 bucket for our log's
#   - loki.tf - provision loki to K8s by helm
#-----------------------------------------------
provider "aws" {
  region = var.region
  default_tags {
    tags = var.default_tags
  }
}
# Collecting data
#-----------------------------------
data "aws_availability_zones" "az" {}
data "aws_iam_policy" "ebs_csi_driver" {
  name = "AmazonEBSCSIDriverPolicy"
}