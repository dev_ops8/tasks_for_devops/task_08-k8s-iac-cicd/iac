#----------------------------------------------
# Create S3 bucket and kms key for encrypting
#----------------------------------------------
# Create AWS KMS KEY
resource "aws_kms_key" "kms" {
  description              = "AWS KMS key used to encrypt AWS ${terraform.workspace}-env resources"
  key_usage                = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = 7
}

# Create S3 bucket
resource "aws_s3_bucket" "loki" {
  bucket = "${var.deployment_prefix}-${terraform.workspace}-grafana-loki-storage"
  tags = {
    "Name"        = "${var.deployment_prefix}-${terraform.workspace}-grafana-loki-storage"
    "Description" = "Grafana Loki Storage for deployment: ${var.deployment_prefix}-${terraform.workspace}"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "loki" {
  bucket = aws_s3_bucket.loki.id
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.kms.arn
      sse_algorithm     = "aws:kms"
    }
    bucket_key_enabled = true
  }
}

resource "aws_s3_bucket_acl" "loki" {
  bucket = aws_s3_bucket.loki.id
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "loki" {
  bucket                  = aws_s3_bucket.loki.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}