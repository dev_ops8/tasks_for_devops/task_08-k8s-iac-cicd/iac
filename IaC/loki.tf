#----------------------------------------
# Provision loki in our K8s cluster
#----------------------------------------
# Descript provider Helm
provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      # This requires the awscli to be installed locally where Terraform is executed
      args = ["eks", "get-token", "--cluster-name", module.eks.cluster_id]
    }
  }
}

# Loki-stack Helm
resource "helm_release" "loki" {
  name             = "loki"
  repository       = "https://grafana.github.io/helm-charts"
  chart            = "loki-stack"
  version          = "2.8.4"
  namespace        = local.loki.namespace
  create_namespace = true
  values = [
    templatefile("${path.module}/templates/loki-values.yaml", {
      sa_name     = local.loki.service_account_name,
      role_arn    = module.irsa_loki.iam_role_arn,
      region      = aws_s3_bucket.loki.region,
      bucket_name = aws_s3_bucket.loki.id
    })
  ]
  depends_on = [module.eks]
}