#--------------------------------------
# Creatie IAM role of container storage
# interface for our pod's
#--------------------------------------
locals {
  ebs_csi_driver = {
    namespace            = "kube-system"
    service_account_name = "ebs-csi-controller-sa"
  }
}

module "irsa_ebs_csi_driver" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "5.3.0"
  create_role                   = true
  role_name                     = "${var.cluster_name}-${terraform.workspace}-ebs-csi-driver-role"
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [data.aws_iam_policy.ebs_csi_driver.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.ebs_csi_driver.namespace}:${local.ebs_csi_driver.service_account_name}"]
}

# Experiment try to add arn of role to K8s .yaml file 
# resource "local_file" "role_arn_to_k8s" {
#   template = file("${path.module}/../K8s/pod's.yaml") # change path to yaml file
#   vars = {
#     role_s3_arn = module.irsa_ebs_csi_driver.iam_role_arn
#   }
# }