#------------------------------------
# Creating VPC and subnet's
#------------------------------------
# VPC
module "vpc" {
  source                 = "terraform-aws-modules/vpc/aws"
  version                = "3.18.0"
  name                   = "${var.deployment_prefix}-${terraform.workspace}-VPC"
  cidr                   = var.cidr_range[0]
  azs                    = [data.aws_availability_zones.az.names[0], data.aws_availability_zones.az.names[1]]
  private_subnets        = var.vpc_private_subnets
  public_subnets         = var.vpc_public_subnets
  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false
  enable_dns_hostnames   = true
  tags = {
    "Name" = "${var.deployment_prefix}-${terraform.workspace}-VPC"
  }
  public_subnet_tags = {
    "Name"                                                             = "public-subnet-${var.deployment_prefix}-${terraform.workspace}"
    "kubernetes.io/cluster/${var.cluster_name}-${terraform.workspace}" = "shared"
    "kubernetes.io/role/elb"                                           = "1"
  }
  private_subnet_tags = {
    "Name"                                                             = "private-subnet-${var.deployment_prefix}-${terraform.workspace}"
    "kubernetes.io/cluster/${var.cluster_name}-${terraform.workspace}" = "shared"
    "kubernetes.io/role/internal-elb"                                  = "1"
  }
  public_route_table_tags = {
    "Name" = "public-route-table-${var.deployment_prefix}-${terraform.workspace}"
  }
  private_route_table_tags = {
    "Name" = "private-route-table-${var.deployment_prefix}-${terraform.workspace}"
  }
}