variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "dynamodb_table" {
  type    = string
  default = "terraform-states-lock"
}

variable "state_bucket" {
  type    = string
  default = "terraform-states-28112022"
}

variable "cluster_name" {
  type    = string
  default = "K8s-Web-app"
}

variable "inst_type" {
  type    = string
  default = "t2.medium"
}

variable "deployment_prefix" {
  type    = string
  default = "k8s"
}

variable "cidr_range" {
  type    = list(string)
  default = ["10.1.0.0/16"]
}

variable "vpc_private_subnets" {
  type    = list(string)
  default = ["10.1.0.0/19", "10.1.32.0/19"]
}

variable "vpc_public_subnets" {
  type    = list(string)
  default = ["10.1.96.0/22", "10.1.100.0/22"]
}

variable "default_tags" {
  type        = map(string)
  description = "Default tags for AWS that will be attached to each resource."
  default = {
    "Environment" = "Development",
    "Team"        = "DevOps",
    "DeployedBy"  = "Terraform"
  }
}