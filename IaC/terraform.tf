terraform {
  # backend "s3" {
  #   bucket         = "terraform-states-28112022"
  #   key            = "K8s-Web-app/terraform.tfstate"
  #   region         = "eu-central-1"
  #   dynamodb_table = "terraform-states-lock"
  #   encrypt        = true
  # }
  required_providers {
    aws = {
      version = "4.20.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.7.0"
    }
  }
  required_version = ">=0.13"
}