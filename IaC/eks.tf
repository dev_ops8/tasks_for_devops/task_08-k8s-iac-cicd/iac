#--------------------------------------
# Create K8s cluster
#--------------------------------------
module "eks" {
  source                          = "terraform-aws-modules/eks/aws"
  version                         = "18.30.2"
  cluster_name                    = "${var.cluster_name}-${terraform.workspace}"
  cluster_version                 = "1.23"
  vpc_id                          = module.vpc.vpc_id
  subnet_ids                      = module.vpc.private_subnets
  enable_irsa                     = true
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true # question is this nedeed ? it will be needed when we want to connect from PC host
  create_cloudwatch_log_group     = false
  cluster_enabled_log_types       = []
  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = "v1.8.7-eksbuild.2"
    }
    kube-proxy = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = "v1.23.8-eksbuild.2"
    }
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
      addon_version     = "v1.11.4-eksbuild.1"
    }
    aws-ebs-csi-driver = {
      resolve_conflicts        = "OVERWRITE"
      addon_version            = "v1.13.0-eksbuild.2"
      service_account_role_arn = module.irsa_ebs_csi_driver.iam_role_arn
    }
  }
  tags = {
    "Name"            = "${var.cluster_name}-${terraform.workspace}"
    "Type"            = "Kubernetes Service"
    "K8s Description" = "Kubernetes for deployment"
  }

  # EKS Managed Node Group(s)
  eks_managed_node_groups = {
    management = {
      min_size       = 2
      max_size       = 4
      desired_size   = 2
      instance_types = ["${var.inst_type}"]
      capacity_type  = "ON_DEMAND"
      block_device_mappings = {
        xvda = {
          device_name = "/dev/xvda"
          ebs = {
            volume_size           = 20
            volume_type           = "gp3"
            encrypted             = true
            delete_on_termination = true
          }
        }
      }
      labels = {
        "node.k8s/role" = "management"
      }
    }
  }

  # Security rules for nodes and control panel
  node_security_group_additional_rules = {
    ingress_allow_access_from_control_plane = {
      type                          = "ingress"
      protocol                      = "tcp"
      from_port                     = 1025
      to_port                       = 65535
      source_cluster_security_group = true
      description                   = "Allow workers pods to receive communication from the cluster control plane."
    }
    ingress_self_all = {
      description = "Allow ingress traffic from nodes to communicate with each other (all ports/protocols)."
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    egress_all = {
      description      = "Allow egress traffic to communicate with each other (all ports/protocols)."
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }
}